﻿using System;
using System.Collections.Generic;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set up collection
            List<Animal> animals = new List<Animal>();

            // Populate collection with animal objects
            animals.Add(new Animal("Human"));                                   // Using the simple constructor
            animals.Add(new Animal("Dog", "Bob", 10, 40.2, 1.2));               // Using the more comprehensive constructor
            animals.Add(new Animal("Cat", "Lucy", 2, 4.2, 0.35));


            // Go through all the animals. If its a human, have it make a sound
            foreach(Animal a in animals)
            {
                if(a.isHuman())
                {
                    a.MakeSound();
                }
            }
        }
    }
}
