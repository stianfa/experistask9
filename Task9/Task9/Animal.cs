﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class Animal
    {
        public string Species { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }


        public Animal(string species)
        {
            Species = species;
        }

        public Animal(string species, string name, int age, double weight, double height)
        {
            Species = species;
            Name = name;
            Age = age;
            Weight = weight;
            Height = height;
        }

        public void MakeSound()
        {
            Console.WriteLine("GJRIJREIWEFJIWEO");
        }

        public void EatFood(double amount)
        {
            Console.WriteLine("mmmmm....goood food!");
            Weight += amount;
        }

        public bool IsHuman()
        {
            return Species.Equals("Human");
        }
    }
}
